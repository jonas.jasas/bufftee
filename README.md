# Buffered and thread safe* tee

BuffTee is a Go package that buffers stream data and makes it available for multiple readers.
Readers will start to receive data as soon as it appears in the buffer.
Writing and reading can be performed on separate subroutines.
 
* Thread safe*
* Multiple **non-blocking** readers
* Fast buffering (using linked data structure) 
* Readable after writing is finished 
* Cancellable

[![go report card](https://goreportcard.com/badge/gitlab.com/jonas.jasas/bufftee)](https://goreportcard.com/report/gitlab.com/jonas.jasas/bufftee)
[![pipeline status](https://gitlab.com/jonas.jasas/bufftee/badges/master/pipeline.svg)](https://gitlab.com/jonas.jasas/bufftee/commits/master)
[![coverage report](https://gitlab.com/jonas.jasas/bufftee/badges/master/coverage.svg)](https://gitlab.com/jonas.jasas/bufftee/commits/master)
[![godoc](https://godoc.org/gitlab.com/jonas.jasas/bufftee?status.svg)](http://godoc.org/gitlab.com/jonas.jasas/bufftee)

\* Writing and reading can be done on separate subroutines.
Separate reader instances can be used on separate subroutines.  

## When to use BuffTee?

* Multiple stream data consumers
* Data must be available after transfer is finished


## Installation

Simple install the package to your [$GOPATH](https://github.com/golang/go/wiki/GOPATH "GOPATH") with the [go tool](https://golang.org/cmd/go/ "go command") from shell:
```bash
$ go get gitlab.com/jonas.jasas/bufftee
```
Make sure [Git is installed](https://git-scm.com/downloads) on your machine and in your system's `PATH`.

## Example

Example show how to create `BuffTee` and use it with two consumers on separate go routines.

```go
bt := bufftee.NewBuffTee(nil)

reader1 := bt.NewReader()
go myFirstConsumer(reader1)  // Do things with your data on separate routine

reader2 := bt.NewReader()
go mySecondConsumer(reader2) // Do other things with your data on separate routine

io.Copy(bt, source)
bt.Close()  // Important! Marking that no more data will be added
```

Working example can be found in [examples](examples/counter) directory.