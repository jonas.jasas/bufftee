package bufftee

import "unsafe"

type block struct {
	data     []byte
	next     *block
	nextChan chan struct{}
}

func newBlock(b []byte) *block {
	s := &block{
		data:     make([]byte, len(b)),
		nextChan: make(chan struct{}),
	}

	copy(s.data, b)
	return s
}

func (b *block) memory() int64 {
	structSize := uint64(unsafe.Sizeof(b))
	dataSize := len(b.data)
	return int64(structSize) + int64(dataSize)
}
