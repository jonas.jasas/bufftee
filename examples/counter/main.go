package main

import (
	"fmt"
	"gitlab.com/jonas.jasas/bufftee"
	"io"
	"net/http"
	"sync"
)

// Example illustrates use of BuffTee. (example is highly inefficient and made just for showing how to use BuffTee)
// Finds letter count in http://example.com/ page.
func main() {
	if res, err := http.DefaultClient.Get("http://example.com/"); err == nil && res.StatusCode == 200 {
		bt := bufftee.NewBuffTee(nil)
		wg := sync.WaitGroup{}

		for i := 97; i < 123; i++ { // Iterating from "a" to "z"
			wg.Add(1)
			go func(i int) { // Starting separate routine to count each letter
				r := bt.NewReader()
				b := make([]byte, 10)
				var occ int
				for {
					n, err := r.Read(b)
					if err != nil {
						break
					}
					for _, s := range b[:n] {
						if s == byte(i) {
							occ++
						}
					}
				}
				fmt.Printf("%s = %d\n", string(i), occ)
				wg.Done()
			}(i)
		}

		io.Copy(bt, res.Body)
		bt.Close() // Important! Must explicitly tell that no more data is coming
		wg.Wait()
	} else {
		fmt.Println("Unsuccessful request")
	}
}
