package bufftee

import "io"

// TeeReader is a result type of BuffTee.NewReader() .
// It allows reading same stream multiple times.
type TeeReader struct {
	last       *block
	cancelChan <-chan struct{}
	offset     int
}

// Read implements Reader interface.
// Starts reading buffered data from the beginning.
func (tr *TeeReader) Read(b []byte) (n int, err error) {
	select {
	case <-tr.last.nextChan:
		if tr.last.next == nil {
			return 0, io.EOF
		}

		n = copy(b, tr.last.next.data[tr.offset:])
		tr.offset += n

		if len(tr.last.next.data) == tr.offset {
			tr.offset = 0
			tr.last = tr.last.next
		}
	case <-tr.cancelChan:
		return 0, io.ErrClosedPipe
	}
	return
}
