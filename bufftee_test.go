package bufftee

import (
	"bytes"
	"gitlab.com/jonas.jasas/rwmock"
	"io"
	"math/rand"
	"testing"
	"time"
)

func TestConsumers(t *testing.T) {
	bt := NewBuffTee(nil)

	const consumerCnt = 100

	resChan := make(chan []byte)
	for i := 0; i < consumerCnt; i++ {
		go func() {
			btr := bt.NewReader()
			shr := rwmock.NewShaperRand(btr, 1, 10000, 0, time.Microsecond)
			bb := &bytes.Buffer{}
			io.Copy(bb, shr)

			resChan <- bb.Bytes()
		}()

		time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
	}

	equivalent := make([]byte, 10000000)
	rand.Read(equivalent)
	r := bytes.NewReader(equivalent)
	shr := rwmock.NewShaperRand(r, 1, 10000, 0, time.Microsecond)

	io.Copy(bt, shr)
	bt.Close()

	for i := 0; i < consumerCnt; i++ {
		resb := <-resChan
		if bytes.Compare(equivalent, resb) != 0 {
			t.FailNow()
		}
	}
}

func TestCancellation(t *testing.T) {
	cancelChan := make(chan struct{})
	bt := NewBuffTee(cancelChan)

	const consumerCnt = 100

	resChan := make(chan error)
	for i := 0; i < consumerCnt; i++ {
		go func() {
			btr := bt.NewReader()
			shr := rwmock.NewShaperRand(btr, 1, 10000, 0, 0)
			bb := &bytes.Buffer{}
			_, err := io.Copy(bb, shr)

			resChan <- err
		}()

		time.Sleep(time.Millisecond * time.Duration(rand.Intn(100)))
	}

	equivalent := make([]byte, 1000000)
	rand.Read(equivalent)
	r := bytes.NewReader(equivalent)
	shr := rwmock.NewShaperRand(r, 1, 10000, 0, 0)

	io.Copy(bt, shr)
	close(cancelChan)

	for i := 0; i < consumerCnt; i++ {
		resErr := <-resChan
		if resErr != io.ErrClosedPipe {
			t.FailNow()
		}
	}
}

func TestSize1(t *testing.T) {
	bt := NewBuffTee(nil)

	size := 1000000
	equivalent := make([]byte, size)
	rand.Read(equivalent)
	r := bytes.NewReader(equivalent)
	shr := rwmock.NewShaperRand(r, 1, 10000, 0, 0)
	io.Copy(bt, shr)
	bt.Close()
	if bt.Size() != size {
		t.FailNow()
	}
}

func TestSize2(t *testing.T) {
	bt := NewBuffTee(nil)

	size := 1234567
	equivalent := make([]byte, size)
	rand.Read(equivalent)
	r := bytes.NewReader(equivalent)
	shr := rwmock.NewShaperRand(r, 1, 10000, 0, 0)
	io.Copy(bt, shr)

	if bt.Size() != size {
		t.FailNow()
	}
}

func TestMemory(t *testing.T) {
	bt := NewBuffTee(nil)

	size := 1000000
	equivalent := make([]byte, size)
	rand.Read(equivalent)
	r := bytes.NewReader(equivalent)
	io.Copy(bt, r)

	if bt.Memory() <= int64(size) {
		t.FailNow()
	}

	bt.Close()

	if bt.Memory() <= int64(size) {
		t.FailNow()
	}
}

func TestWriteOnClosed(t *testing.T) {
	size := 1000000
	equivalent := make([]byte, size)
	rand.Read(equivalent)

	bt := NewBuffTee(nil)

	if n, err := bt.Write(equivalent); n != size || err != nil {
		t.Fail()
	}

	bt.Close()

	if _, err := bt.Write(equivalent); err != io.ErrClosedPipe {
		t.Fail()
	}
}
