package bufftee

import (
	"io"
	"sync"
)

// BuffTee is a Go package that buffers stream data and makes it available for multiple readers.
// Readers will start to receive data as soon as it appears in the buffer.
// Writing and reading can be performed on separate subroutines.
type BuffTee struct {
	first      *block
	last       *block
	cancelChan <-chan struct{}
	sync.Mutex
}

// NewBuffTee creates new BuffTee instance.
// cancelChan - a channel that when it is closed it signals that transmission is aborted.
func NewBuffTee(cancelChan <-chan struct{}) *BuffTee {
	s := newBlock(nil)
	if cancelChan == nil {
		cancelChan = make(chan struct{})
	}
	return &BuffTee{
		first:      s,
		last:       s,
		cancelChan: cancelChan,
	}
}

// Write implements Writer interface.
// Write can't be called more than once at the same time on the same instance.
func (t *BuffTee) Write(b []byte) (n int, err error) {
	t.Lock()
	defer t.Unlock()

	select {
	case <-t.last.nextChan:
		return 0, io.ErrClosedPipe
	default:
		s := newBlock(b)
		t.last.next = s
		close(t.last.nextChan)
		t.last = s
	}
	return len(b), nil
}

// Close signals to all readers that at this point is EOF.
// Marks that transmission successfully finished.
func (t *BuffTee) Close() error {
	t.Lock()
	defer t.Unlock()

	close(t.last.nextChan)
	return nil
}

// NewReader creates new reader object that can be used on separate subroutine.
func (t *BuffTee) NewReader() *TeeReader {
	return &TeeReader{
		last:       t.first,
		cancelChan: t.cancelChan,
	}
}

// Size returns buffered data size
func (t *BuffTee) Size() (n int) {
	curr := t.first
	for {
		select {
		case <-curr.nextChan:
			if curr.next == nil {
				return
			}
			curr = curr.next
			n += len(curr.data)
		default:
			return
		}
	}
}

// Memory returns memory amount occupied by the buffer.
// Returned value is always greater that data size itself.
func (t *BuffTee) Memory() (n int64) {
	curr := t.first
	for {
		select {
		case <-curr.nextChan:
			if curr.next == nil {
				return
			}
			curr = curr.next
			n += curr.memory()
		default:
			return
		}
	}
}
